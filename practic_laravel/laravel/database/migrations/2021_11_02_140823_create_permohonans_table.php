<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermohonansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permohonans', function (Blueprint $table) {
            $table->id();
            $table->string('sitename');
            $table->string('siteid');
            $table->text('alamat')->nullable();
            $table->string('latitude');
            $table->string('longitude');
            $table->string('tinggi_menara');
            $table->string('kontak');
            $table->string('surat_towerbersama')->nullable();
            $table->string('surat_perubahankepemilikan')->nullable();
            $table->string('surat_kuasa')->nullable();
            $table->string('rekomendasicamat')->nullable();
            $table->string('rekomendasikuwu')->nullable();
            $table->string('izintetangga')->nullable();
            $table->string('foto_menara')->nullable();
            $table->string('ktp')->nullable();
            $table->string('profil_perusahaan')->nullable();
            $table->string('APD')->nullable();
            $table->string('kebutuhan_bts')->nullable();
            $table->string('rekomendasidishub')->nullable();
            $table->string('status')->default('process');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permohonans');
    }
}

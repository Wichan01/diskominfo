<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        //default admin akun
        DB::table('users')->insert([
            [
                'name' => "Admin",
                'email' => "admin@admin.com",
                'is_admin' => 1,
                'password' => Hash::make('admin123'), //passwordnya
            ],
            // [
            //     'name' => "User",
            //     'email' => "user@user.com",
            //     'is_admin' => 0,
            //     'password' => Hash::make('user1234'), //passwordnya
            //     ]
            ]);
        }
    }
    
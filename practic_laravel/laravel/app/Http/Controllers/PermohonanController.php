<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Permohonan;
use App\Models\Profile;
use App\Models\User;


class PermohonanController extends Controller
{
    public function index()
    {
        $user = User::findOrFail(session('id'));
        $profile = Profile::findOrFail($user->id_profile);
        $permohonan = Permohonan::all();
        return view('permohonan.index', ['permohonan' => $permohonan],['profile' => $profile] );
    }
    
    public function create(){
        $user = User::findOrFail(session('id'));
        $profile = Profile::findOrFail($user->id_profile);
        return view('permohonan.create', ['profile' => $profile]);
    }

    public function profil(){
        $user = User::findOrFail(session('id'));
        $profile = Profile::findOrFail($user->id_profile);
        return view('permohonan.profil', ['profile' => $profile]);
    }

    public function store(Request $request)
    {
        $validateData = $request->validate([
            'sitename'                     => 'required',
            'siteid'                       => 'required|min:3|max:100',
            'latitude'                     => 'required',
            'longitude'                    => 'required',
            'tinggi_menara'                => 'required',
            'alamat'                       => 'required',
            'kontak'                       => 'required',
            'ktp'                          => 'required|mimes:jpeg,png,jpg,gif,svg',
            'surat_towerbersama'           => 'required|mimes:pdf',
            'surat_perubahankepemilikan'   => 'required|mimes:pdf',
            'surat_kuasa'                  => 'required|mimes:pdf',
            'rekomendasicamat'             => 'required|mimes:pdf',
            'rekomendasikuwu'              => 'required|mimes:pdf',
            'izintetangga'                 => 'required|mimes:pdf',
            'rekomendasidishub'            => 'required|mimes:pdf',
            'foto_menara'                  => 'required|mimes:jpeg,png,jpg,gif,svg',
            'APD'                          => 'required|mimes:pdf',
            'kebutuhan_bts'                => 'required|mimes:pdf',
        ]);

        $permohonan  = new Permohonan();
        $permohonan ->sitename = $validateData['sitename'];
        $permohonan ->siteid = $validateData['siteid'];
        $permohonan ->latitude = $validateData['latitude'];
        $permohonan ->longitude = $validateData['longitude'];
        $permohonan ->tinggi_menara = $validateData['tinggi_menara'];
        $permohonan ->alamat = $validateData['alamat'];
        $permohonan ->Kontak = $validateData['kontak'];
        // auto
        $permohonan ->status = 'Proses';

        if ($request->hasFile('ktp')
        && $request->hasFile('foto_menara')) {
            $extFile = $request->ktp->getClientOriginalExtension();
            $namaFile = 'ktp-'.time().".".$extFile;
            $path = $request->ktp->move('assets/files/image_permohonan', $namaFile);
            $permohonan ->ktp = $path;

            $extFile = $request->foto_menara->getClientOriginalExtension();
            $namaFile = 'foto_menara-'.time().".".$extFile;
            $path = $request->foto_menara->move('assets/files/image_permohonan', $namaFile);
            $permohonan->foto_menara = $path;
        }
        if ($request->hasFile('surat_towerbersama')
            && $request->hasFile('surat_perubahankepemilikan')
            && $request->hasFile('surat_kuasa')
            && $request->hasFile('rekomendasicamat')
            && $request->hasFile('rekomendasikuwu')
            && $request->hasFile('izintetangga')
            && $request->hasFile('rekomendasidishub')
            && $request->hasFile('APD')
            && $request->hasFile('kebutuhan_bts')
        ){
            $arraydata = array('surat_towerbersama', 'surat_perubahankepemilikan','surat_kuasa', 'rekomendasicamat', 
            'rekomendasikuwu','izintetangga', 'rekomendasidishub','APD', 'kebutuhan_bts');

            foreach ($arraydata as $d) {
                $extFile = $request->$d->getClientOriginalExtension();
                $namaFile = $d.'-'.time().".".$extFile;
                $path = $request->$d->move('assets/files/document_permohonan', $namaFile);
                $permohonan ->$d = $path;
            }
            
        }
        // $updateUser = User::findOrFail($validateData['userEmail']);

        // $updateUser->id_profile = $profile->id;

        // $updateUser->push();

        $permohonan ->save();
        $request->session()->flash('pesan', 'Permohonan Telah Diajukan');
        return redirect()->route('permohonan.index');
    }
}

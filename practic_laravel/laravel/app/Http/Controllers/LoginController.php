<?php

namespace App\Http\Controllers;
use App\Models\Login;
use App\Models\Admin;
use App\Models\Permohonan;
use App\Models\User;
use App\Models\Profile;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    // use AuthenticatesUsers;
    
    // protected $redirectTo = RouteServiceProvider::HOME;

    public function __construct() {
        if(session()->has(['id', 'name', 'email'])) {
            if(session('isAdmin')){
                return redirect()->route('admin.index');
            }else {
                return redirect()->route('permohonan.index');
            }
        }
    }
    
    public function login(){
        
        return view('login.login');
    }
    
    public function poslogin(Request $request) {
        $validateData = $this->validate($request,[
            'email'     => 'required|email',
            'password'  => 'required',
        ]);
        
        $searchUser = User::where('email', $validateData['email'])->first();
        
        if($searchUser){
            if(Hash::check($validateData['password'], $searchUser->password)) {
                // if($validateData['password'] == $searchUser->password) {
                    if($searchUser->is_admin == 1){
                        $permohonan = Profile::findOrFail($searchUser->id_profile);
                        session([
                            'id' => $searchUser->id,
                            'name' => $searchUser->name,
                            'email' => $searchUser->email,
                            'isAdmin' => $searchUser->is_admin == 1,
                            'username' => $permohonan->username
                        ]);
                        return redirect()->route('admin.index');
                    }else{
                        
                        if($searchUser->id_profile != null) {
                            $permohonan = Profile::findOrFail($searchUser->id_profile);
                            session([
                                'id' => $searchUser->id,
                                'name' => $searchUser->name,
                                'email' => $searchUser->email,
                                'isAdmin' => $searchUser->is_admin == 0,
                                'username' => $permohonan->username
                            ]);
                        }else{
                            session([
                                'id' => $searchUser->id,
                                'name' => $searchUser->name,
                                'email' => $searchUser->email,
                                'isAdmin' => $searchUser->is_admin == 0,
                            ]);
                        }
                        return redirect()->route('permohonan.index');
                    }
                }else{
                    return back()->withInput()->with('error', 'Email atau Password Salah');
                }
            }else {
                return back()->withInput()->with('error', 'Email atau Password Salah');
            }
            
        }
        
        // public function poslogin(Request $request)
        // {
            //     $input = $request->all();
            //     $this->validate($request,[
                //         'email'     => 'required|email',
                //         'password'  => 'required',
                
                //     ]);
                
                //     if(auth()->attemp(array('email' => $input['email'], 'password'=> $input['password']))){
                    //         if(auth()->user()->is_admin == 1){
                        //             return redirect()->route('admin.index');
                        //         }else{
                            //             return redirect()->route('permohonan.index');
                            //         }
                            
                            //     } else{
                                //         return redirect()->route('login.login')->with('eror', 'Email dan Password yang anda masukan salah');
                                //     }
                                
                                // }    
                                
                                public function logout()
                                {
                                    // auth()->guard('admin')->logout();
                                    session()->flush();
                                    
                                    return redirect()->route('login.login');
                                }
                                
                            }
                            
